# Apps

[[_TOC_]]

This repository has the ArgoCD apps definition for installing and configuring different cluster services.

All updates are applied automatically, as [base-apps](https://gitlab.com/drapko/kubernetes/argocd/base-apps) repository points to the most recent version (*HEAD*)

## Structure

+ `namespaces`: it should create and configure every new namespace used by any [stack-apps](https://gitlab.com/drapko/kubernetes/argocd/stacks-apps).

+ `common-*`: common configuration for namespaces. Currently, apply configuration for `external-secrets` and `traefik-v2` ingress